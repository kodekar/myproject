package com.network.discovery.server;

public class TestServer {

	public static void main(String[] args) {
		Runnable server = new UDPServer(6888);
		
		System.out.println("Creating one thread of the UDP server");
		Thread thread = new Thread(server);
		thread.start();		
	}

}
