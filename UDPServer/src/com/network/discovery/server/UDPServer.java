package com.network.discovery.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPServer implements Runnable {

	private int port;
	private DatagramSocket socket;

	public UDPServer(int port) {
		this.port = port;
	}

	@Override
	public void run() {
		try {
			// Open a socket to listen to all UDP traffic on the required port
			socket = new DatagramSocket(port, InetAddress.getByName("0.0.0.0"));
			socket.setBroadcast(true);

			while (true) {
				System.out.println(">>> Waiting for packets to be received!!!!");

				// Receive a packet
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);

				// Packet received
				System.out.println(">>> Discovery packet received from: "
						+ packet.getAddress().getHostAddress());
				System.out.println(">>> Packet received; data: " + new String(packet.getData()));

				// Check and respond to the message appropriately
				String message = new String(packet.getData()).trim();
				System.out.println(">>> Message received from client is : " + message );
				//if (message.equals("NETWORK_DISCOVERY_REQUEST")) {
					String responseMessage = "NETWORK_DISCOVERY_RESPONSE";

					// Send a response
					DatagramPacket sendPacket = new DatagramPacket( responseMessage.getBytes(), responseMessage.getBytes().length, packet.getAddress(),
							packet.getPort());
					socket.send(sendPacket);

					System.out.println(
							">>> Sent packet " + responseMessage + " to: " + sendPacket.getAddress().getHostAddress());
				//}
			}
		} catch (IOException ex) {
			System.out.println(">>> Exception ocurred!!!!");
			ex.printStackTrace();
		}
	}
}
