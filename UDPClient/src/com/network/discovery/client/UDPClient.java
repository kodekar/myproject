package com.network.discovery.client;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPClient {

	public static void main(String[] args) {

		int port = 6888;
		DatagramSocket socket;
		// Find the server using UDP broadcast
		try {
			socket = new DatagramSocket();
			// Open a random port to send the package
			socket.setBroadcast(true);

			byte[] sendData = "NETWORK_DISCOVERY_REQUEST".getBytes();
			
			InetAddress broadcast = InetAddress.getByName("192.168.1.255");
			try {
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, port);
				socket.send(sendPacket);
			} catch (Exception e) {

			}
			System.out.println(">>> Request packet sent to: " + broadcast.getHostAddress());

			// for(int i=0; i<=10; i++) {
			// receiveMessage(socket);
			// Thread.sleep(2000);
			// }

			receiveMessage(socket);

			// Close the port!
			socket.close();
		} catch (Exception ex) {
			System.out.println(">>>Exception ocurred:");
			ex.printStackTrace();
		} finally {

		}

	}

	public static void receiveMessage(DatagramSocket socket) throws Exception {

		String message = "";

		while (!message.equals("NETWORK_DISCOVERY_RESPONSE")) {
			// Wait for a response
			byte[] recvBuf = new byte[15000];
			DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
			socket.receive(receivePacket);
			message = new String(receivePacket.getData()).trim();
			
			if (message.equals("NETWORK_DISCOVERY_RESPONSE")) {
				// We have a response
				System.out
						.println(">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

				// Check if the message is correct				
				System.out.println(">>> Message received is " + message);

				// DO SOMETHING WITH THE SERVER'S IP (for example, store it in your controller)
				System.out.println(">>> IP Address of the Server is : " + receivePacket.getAddress());
			}
		}
	}
}
