//==================================================================================================
//                IR Blaster
//==================================================================================================
#ifndef UNIT_TEST
#include <Arduino.h>
#endif
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <EEPROM.h>      //For EEEPROM library inclusion
#include <ESP8266WiFi.h> //For ESP wifi library
#include <WiFiUdp.h>     // To enable UDP over wifi
#include <StringTokenizer.h>
#include <WebSocketsServer.h>
#include <Hash.h>
#include <string.h>
#include <Ticker.h>
#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h> //MQTT client PubSub

extern "C"
{
#include "spi_flash.h"
#include "ets_sys.h"
}

#define DEBUG 0
#define SERIAL_DEBUG 0
#define EEPROM_SIZE 512
#define FACTORY_RESET_PIN 2 //ESP8266 GPI0-2 PIN is D4
#define UDP_PORT 6755
#define TCP_PORT 9999

#define LED_COUNT 20
#define LED_PIN 4 //ESP8266 GPI0-4 is D2

char ap_ssid[30] = {'C', 'o', 'n', 'n', 'e', 'c', 't', 'e', 'd', 'A', 'P', '-'};
char *ap_password = "123456789";

char packetBuffer[UDP_TX_PACKET_MAX_SIZE];
WiFiUDP udp;
IPAddress apIP(192, 168, 7, 1);
IPAddress ipMulti(239, 255, 255, 255);

Ticker flipper;

enum Eeprom_Data_address {
        INITIALIZED = 0,
        NO_OF_RELAY = 1,
        COLOR = 2,//PWM active or not
        COLOR_ONOFF = 3,
        COLOR_NAME = 4, //Name of the device in room
        COLOR_INTENSITY = 5, // 255x 1/2/3/4
        COLOR_R = 6, //DutyCycle
        COLOR_G = 7,
        COLOR_B = 8,
        RELAY1 = 9, //add 7 to accomodate following data Active : on/off : type : intensity : R : G : B
        RELAY2 = 16,
        RELAY3 = 23,
        RELAY4 = 30,
        RELAY5 = 37,
        RELAY6 = 44,
        RELAY7 = 51,
        RELAY8 = 58,
        MQTTCONFIG = 65
};


IRsend irsend(4); // An IR LED is controlled by GPIO pin 4 (D2)
WebSocketsServer webSocket = WebSocketsServer(TCP_PORT);
WiFiUDP ntpUDP;
#define CONFIG_WIFI_SECTOR 0x7E

String TxRx_string, temp, currentIP;
String Parsed_data[5]; //Max number of commad data is 5
String IRCODE[3];
String DEVICE_INIT[2];
String mode_ssid_pswd[4];

String mqttServer;
String mqttServerUsername;
String mqttServerPassword;
unsigned int mqttServerPort;
String mqtt_config_parameters[5];
long lastReconnectAttempt = 0;

String requestRoutingKey;
String responseRoutingKey;
String responseRoutingKeySuffix = "_RESPONSE";
String queueNamePrefix = "ConnectedSmartLife::ConnectedLite::Client-";

WiFiClient espWiFiClient;
PubSubClient mqttClient(espWiFiClient);

// =================================================================================================
// ------------------------------------------------ End of declarations ----------------------------
// =================================================================================================

//==================================================================================================
//  Function: Clear Memory
//       This clears the entire EEPROM memory
// =================================================================================================
void eraseEEPROM()
{
  #if DEBUG
        Serial.println("Writing string");
  #endif
        EEPROM.write(INITIALIZED, DEVICE_INIT[1].toInt());
        EEPROM.commit();
        EEPROM.write(MQTTCONFIG, '\0');
        EEPROM.commit();

        //Make everything zero before setting default value
        for (int i = 0; i < EEPROM_SIZE; i++)
                EEPROM.write(i, 0);
        EEPROM.commit();
}

void config_init_default() {
        ETS_UART_INTR_DISABLE();
        spi_flash_erase_sector(CONFIG_WIFI_SECTOR);
        ETS_UART_INTR_ENABLE();
}

// ***************************************************************************
// EEPROM helper
// ***************************************************************************
void write_String(unsigned int add, String data) {

// #if DEBUG
//         Serial.println("Writing string");
// #endif
        int _size = data.length();
        int i;
        for (i = 0; i < _size; i++) {
                EEPROM.write(add + i, data[i]);
                EEPROM.commit();
        }
        EEPROM.write(add + _size, '\0'); //Add termination null character for String Data
        EEPROM.commit();
// #if DEBUG
//         Serial.println("Writing EOS character at");
//         Serial.println(add + _size);
// #endif
}

String read_String(unsigned int add)
{
// #if DEBUG
//           Serial.println("Reading string from address");
//           Serial.println(add);
// #endif
        int i;
        char data[100]; //Max 100 Bytes
        int len = 0;
        unsigned char k;
        k = EEPROM.read(add);

        while (k != '\0' && len < 100) { //Read until null character
// #if DEBUG
//           Serial.println("reading address");
//           Serial.println(add + len);
// #endif
                k = EEPROM.read(add + len);
                data[len] = k;
                len++;
        }
        data[len] = '\0';
        return String(data);
}
// =================================================================================================
// Function : Command_AP_STA_Config
// Description: This function set the configuration of AP or STA
// =================================================================================================
void Command_AP_STA_Config(String modem_mode_ssid_pswd) {
        int Number_Of_Token = 0;
        StringTokenizer tokens_mode_ssid_pswd(modem_mode_ssid_pswd, "=");
        while (tokens_mode_ssid_pswd.hasNext()) {
                // prints the next token in the string
                mode_ssid_pswd[Number_Of_Token] = tokens_mode_ssid_pswd.nextToken();
#if DEBUG
                Serial.println(mode_ssid_pswd[Number_Of_Token]);
#endif
                Number_Of_Token++;
        }
        if (mode_ssid_pswd[1] == "AP")
                ;
        else if (mode_ssid_pswd[1] == "STA") {

                temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=LITE_SENT=OK";
                webSocket.broadcastTXT(temp);
                temp = "";
                delay(1000);
                delay(2000);
                WiFi.mode(WIFI_STA);
                WiFi.begin(mode_ssid_pswd[2].c_str(), mode_ssid_pswd[3].c_str());
                while (WiFi.status() != WL_CONNECTED)  {
                        delay(500);
#if DEBUG
                        Serial.print("Wifi Status Code : ");
                        Serial.println(WiFi.status());
#endif
                }
                delay(2000);
        }
        ESP.reset();
}

// =================================================================================================
// Function : Command_Factory_Reset
// Description: This function do factory reset
// =================================================================================================
void Command_Factory_Reset(String factory_reset)
{
#if DEBUG
        Serial.println("+ Command_Factory_Reset");
#endif
        // Disconnecting the WiFi Connection and resetiing the EEPROM, forcing to start in the AP mode
        WiFi.disconnect();
        String reset = "reset";
        if (strcmp( factory_reset.c_str(), reset.c_str() ) )
        {
                eraseEEPROM(); //Erase EEPROM memory
                delay(1000);
                config_init_default(); //Reset the config values both AP and STA
        }

        temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=LITE_SENT=OK";
        webSocket.broadcastTXT(temp);
        temp = "";
        delay(1000);

        ESP.reset();
#if DEBUG
        Serial.println("- Command_Factory_Reset");
#endif
}

// =================================================================================================
// Function :
// Description: This function Parse the command and call the respective function
// =================================================================================================
void Command_IRBlasting(String IRcode) {
        unsigned short int Number_Of_Token = 0;
        String temp_data = "";
        unsigned int temp_data_int = 0;
        uint16_t *temp_irdata = NULL;
        unsigned short int kHz = 0;
        StringTokenizer tokens(IRcode, "=");
        while (tokens.hasNext()) {
                // prints the next token in the string
                IRCODE[Number_Of_Token] = tokens.nextToken();
#if DEBUG
                Serial.println(IRCODE[Number_Of_Token]);
#endif
                Number_Of_Token++;
        }
        Number_Of_Token = IRCODE[3].toInt();
        kHz = (unsigned short int)IRCODE[1].toInt();
        temp_irdata = new uint16_t[Number_Of_Token];
        if (!temp_irdata)  {
#if DEBUG
                Serial.println("No memory to allocate ... you are going to die");
#endif
                temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=LITE_SENT=ERROR";
                webSocket.broadcastTXT(temp);
                temp = "";
        }
        else  {
                StringTokenizer tokens_ircode_final(IRCODE[2], ",");
                Number_Of_Token = 0;
                while (tokens_ircode_final.hasNext())
                {
                        temp_data_int = tokens_ircode_final.nextToken().toInt();
                        temp_irdata[Number_Of_Token] = ((unsigned int)temp_data_int * 1000 / kHz);
                        Number_Of_Token++;
                }
                irsend.sendRaw(temp_irdata, Number_Of_Token, kHz);
                delete[] temp_irdata;
                temp_irdata = NULL;
        }
        IRCODE[0] = "";
        IRCODE[1] = "";
        IRCODE[2] = ""; //Make the string NULL
        temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=LITE_SENT=OK";
        webSocket.broadcastTXT(temp);
        temp = "";
}

void Command_Set_Initialized(String device_initialized) {
        int Number_Of_Token = 0;
        StringTokenizer tokens(device_initialized, "=");
        while (tokens.hasNext()) {
                // prints the next token in the string
                DEVICE_INIT[Number_Of_Token] = tokens.nextToken();
#if DEBUG
                Serial.println(DEVICE_INIT[Number_Of_Token]);
#endif
                Number_Of_Token++;
        }
        EEPROM.write(INITIALIZED, DEVICE_INIT[1].toInt());
        EEPROM.commit();
        DEVICE_INIT[0] = "";
        DEVICE_INIT[1] = ""; //Make the string NULL
        temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=LITE_SENT=OK";
        webSocket.broadcastTXT(temp);
        temp = "";
}

//==================================================================================================
//  Function: Validate the incoming message
// =================================================================================================
bool validateIncomingMessage(String message) {

        StringTokenizer commandTokens(message, "=");
        String commands[2];
        unsigned int index = 0;
        while( commandTokens.hasNext() ) {
                commands[index] = commandTokens.nextToken();
                index++;
        }
        String queryCommand = "OPSTA?";
        String commandPrefix = "OP";
        String commandOn = "ON";
        String commandOff = "OFF";

#if DEBUG
                Serial.print("Commands size is 2 and commands are ");
                Serial.print(commands[0]);
                Serial.print(" and ");
                Serial.println(commands[1]);
#endif

        if ( strcmp(commands[0].c_str(), queryCommand.c_str() ) && commands[1] == NULL ) {
#if DEBUG
                Serial.println(commands[0].c_str());
                Serial.println(queryCommand.c_str() );
                Serial.println("Commands size is 1 and command is OPSTA?");
#endif
                return true;
        }

        if ( strcmp(commands[1].c_str(),commandOn.c_str()) ||  strcmp(commands[1].c_str(),commandOff.c_str()) ) {
                if(commands[0].length() == 3) {
                        String prefix = commands[0].substring(0,2);
                        if ( strcmp( prefix.c_str(),commandPrefix.c_str() ) ) {
                                String relayNumberStr = commands[0].substring(2,1);
                                int relayNumber = relayNumberStr.toInt();
                                if ( relayNumber >= 1 && relayNumber <= 4 ) {
                                        return true;
                                }
                        }
                }
        }

        return true;

}

// =================================================================================================
// Function : Command_LoadControl
// Description: This function act as a transparent mode
// This gets called from Command Parsing & MQTT Callback
// =================================================================================================
void Command_LoadControl(String receivedMessage)
{
        webSocket.broadcastTXT("Message Received (data sent to serial port) " + receivedMessage);
        if (validateIncomingMessage(receivedMessage)) {
#if DEBUG
        Serial.print("It is a valid message");
#endif
                Serial.println(receivedMessage);
        }
}


//==================================================================================================
//  Function: MQTT Message Callback
// =================================================================================================

void mqttMessageCallback(char* topic, byte* payload, unsigned int length) {

#if DEBUG
        Serial.printf("Message received MQTT broker:");
#endif

        String receivedMessage;

        for (int i = 0; i < length; i++) {
                receivedMessage += (char)payload[i];
        }
        //receivedMessage += '\0';

#if DEBUG
        Serial.printf("%s\n",receivedMessage.c_str());
#endif
        //Validate and send the received message to serial port
        Command_LoadControl(receivedMessage);
}


//==================================================================================================
//  Function: Establish MQTT Connection
// =================================================================================================
bool mqttClientConnect() {

#if DEBUG
        Serial.print("Attempting MQTT connection...");
#endif
        // Create a random client ID
        String clientId = queueNamePrefix + String(random(0xfffffff), HEX);

        // Attempt to connect
        if (mqttClient.connect(clientId.c_str(), mqttServerUsername.c_str(), mqttServerPassword.c_str()) ) {
                requestRoutingKey = WiFi.macAddress();
                responseRoutingKey = WiFi.macAddress()+responseRoutingKeySuffix;

#if DEBUG
                Serial.println("Connected to MQTT Server");
                Serial.printf("Request routing Key : %s\n", requestRoutingKey.c_str() );
                Serial.printf("Response routing Key : %s\n", responseRoutingKey.c_str() );
#endif
                mqttClient.subscribe(requestRoutingKey.c_str(),1);
        } else {
#if DEBUG
                Serial.print("Failed to connect to MQTT Server, RC=");
                Serial.print(mqttClient.state());
                Serial.println(" try again in 10 seconds");
#endif
        }
        return mqttClient.connected();
}
//==================================================================================================
//  Function: Establish MQTT Connection
//  doReset is set only when the configuration is setup for the first time and hence it resets.
//
//  During startup the load of configuration is also hanled by same methog, hence during subsequent
//  loads the reset should not be called, it should just load and connect to server.
//
//  The mqttClient.loop() is done in the loop method with conditions so that it gets trigerred only
//  if the connection is already there
// =================================================================================================
void Command_MQTTConfig(String mqttConfig, unsigned int doReset) {
#if DEBUG
        Serial.printf("Mqtt Config Parameters received in function :");
        Serial.println(mqttConfig);
#endif
        unsigned int tokenCount = 0;
        StringTokenizer mqttConfigTokens(mqttConfig, "=");

        if ( doReset == 1 ) { // Write MQTT configuration only during setup not everytime
                write_String(MQTTCONFIG, mqttConfig);
#if DEBUG
                Serial.println("Reading MQTT Properties from EEPROM immediately after storing");
                Serial.println(read_String(MQTTCONFIG));
#endif
        }
        while (mqttConfigTokens.hasNext()) {
                mqtt_config_parameters[tokenCount] = mqttConfigTokens.nextToken();
                tokenCount++;
        }

        mqttServer = mqtt_config_parameters[1];
        unsigned int port = (unsigned int) strtoul( mqtt_config_parameters[1].c_str(), NULL, 10 );
        mqttServerPort = mqtt_config_parameters[2].toInt();
        mqttServerUsername = mqtt_config_parameters[3];
        mqttServerPassword = mqtt_config_parameters[4];

        // Reset only it is exected when configuration is set for the first time, not during startup or set-up
        // If WiFi is not connected then do not reset. Since Wifi set will anyway reset later
        if ( doReset == 1 ) {

#if DEBUG
                Serial.println("Resetting the module");
                delay(2000);
#endif
                ESP.reset();
        }

        if ( doReset == 0 ) // This is called only from setup so that the connection is established
        {
                //Connecting to RabbitMQ server for MQTT Communications
#if DEBUG
                Serial.println("Connecting to RabbitMQ server for MQTT Communications");
#endif
                mqttClient.setServer(mqttServer.c_str(), mqttServerPort);
                mqttClient.setCallback(mqttMessageCallback);
                delay(5000);
#if DEBUG
                if (mqttClient.connected()) Serial.println("Connected to MQTT server.");
#endif

        }


}

// =================================================================================================
// Function : Command_Parsing
// Description: This function Parse the command and call the respective function
// =================================================================================================
void Command_Parsing() {
        int Number_Of_Token = 0;
        StringTokenizer tokens(TxRx_string, "::");
        while (tokens.hasNext()) {
                // prints the next token in the string
                Parsed_data[Number_Of_Token] = tokens.nextToken();
#if DEBUG
                Serial.println(Parsed_data[Number_Of_Token]);
#endif
                Number_Of_Token++;
        }
        if (Parsed_data[1] != "CMD") {
                temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=WRONG_COMMAND";
                webSocket.broadcastTXT(temp);
                temp = "";
#if DEBUG
                Serial.println("Wrong Command");
#endif
        }
        else {
                if (Parsed_data[2] == "CONF" || Parsed_data[2] == "CONT" || Parsed_data[2] == "STATUS") {
                        if (Parsed_data[3] == "000")
                                ; //Command_AP_STA_Config(Parsed_data[4]);
                        else if (Parsed_data[3] == "001")
                                ; //Command_AP_STA_Config(Parsed_data[4]);
                        else if (Parsed_data[3] == "003")
                                Command_AP_STA_Config(Parsed_data[4]);
                        else if (Parsed_data[3] == "005")
                                Command_Set_Initialized(Parsed_data[4]);
                        else if (Parsed_data[3] == "200")
                                Command_IRBlasting(Parsed_data[4]);
                        else if (Parsed_data[3] == "999")
                                Command_Factory_Reset(Parsed_data[4]);
                        else if(Parsed_data[3] == "011")
                                Command_LoadControl(Parsed_data[4]); // Process incoming message
                        else if (Parsed_data[3] == "311")
                                Command_MQTTConfig(Parsed_data[4], 1);
                }

                Parsed_data[0] = "";
                Parsed_data[1] = "";
                Parsed_data[2] = "";
                Parsed_data[3] = "";
                Parsed_data[4] = "";
        }
}



void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length) {
        switch (type) {

        case WStype_DISCONNECTED:
#if DEBUG
                Serial.printf("[%u] Disconnected!\n", num);
#endif
                break;


        case WStype_CONNECTED: {
                IPAddress ip = webSocket.remoteIP(num);
#if DEBUG
                Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
#endif
                webSocket.broadcastTXT("Connected New Client");
        }

        break;


        case WStype_TEXT:
#if DEBUG
                Serial.printf("[%u] get Text: %s\n", num, payload);
#endif
                TxRx_string = String((char *)payload);
                //parse command based on data received.
                Command_Parsing();
                // send data to all connected clients
                TxRx_string = "";
                break;
        }
}

void flip() {

        // Determine the current IP irrespective of AP or STA mode
        if ( WiFi.status() == WL_CONNECTED)
                currentIP = WiFi.localIP().toString().c_str();
        else
                currentIP = WiFi.softAPIP().toString().c_str();

        temp = "CONNECTEDLITE_NODE=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=" + currentIP;
        webSocket.broadcastTXT(temp);
        temp = "";
}

// =================================================================================================
// Function : WiFiEvent
// Description: This function detects all the WiFi Events
// =================================================================================================
void WiFiEvent(WiFiEvent_t event) {
#if DEBUG
        Serial.printf("[WiFi-event] event: %d\n", event);
#endif
        switch (event) {


        case WIFI_EVENT_STAMODE_CONNECTED:
#if DEBUG
                Serial.println("WiFi station connected");
#endif
                break;


        case WIFI_EVENT_STAMODE_GOT_IP:
#if DEBUG
                Serial.println("WiFi station got IP");
                Serial.println("IP address: ");
                Serial.println(WiFi.localIP());
#endif
                break;


        case WIFI_EVENT_STAMODE_DISCONNECTED:
#if DEBUG
                Serial.println("WiFi lost connection");
                webSocket.broadcastTXT("WRONG_USER_PASSWORD");
#endif
                break;


        case WIFI_EVENT_SOFTAPMODE_STACONNECTED:
#if DEBUG
                Serial.println("WiFi AP mode connected with STA active");
                Serial.println(WiFi.localIP());
                Serial.println((IPAddress)WiFi.softAPIP());
#endif

                break;


        case WIFI_EVENT_SOFTAPMODE_STADISCONNECTED:
#if DEBUG
                Serial.println("WiFi AP mode disconnected");
#endif
                break;
        }
}

//==================================================================================================
//  Function: performReset
//       This function will invoke the factory reset to clear EEPROM and WIFI connections
// =================================================================================================
void performReset() {
#if DEBUG
        Serial.println("+ Command_Factory_Reset");
#endif
        Command_Factory_Reset("reset");
}

//==================================================================================================
//  Function: Setup
//       This function setup EEPROM for setting default value
//       along with GPIO pin to check Factory reset and Serial port with 9600 baudrate for debug or
//        connected light with micro controller.
// =================================================================================================
void setup()
{
        flipper.attach(5, flip);
        Serial.begin(9600); //Initialize serial and wait for port to open:
#if SERIAL_DEBUG
        Serial.setDebugOutput(true);
#endif
        while (!Serial) {
                ; //Wait for serial port to connect.
        }


        // Attaching a failing interrupt for factory reset
        attachInterrupt(FACTORY_RESET_PIN, performReset, FALLING);

        //Make the EEPROM available so that we can read the default values
        EEPROM.begin(EEPROM_SIZE); //Initalize the EEPROM

        //Setting the LED pin to output mode
        pinMode(LED_PIN,OUTPUT);

        WiFi.mode(WIFI_AP_STA); //Set to AP STA mode by default.
        WiFi.onEvent(WiFiEvent);
        String temp_ssid = WiFi.macAddress().c_str();
        strcpy(ap_ssid+12, temp_ssid.substring(0,5).c_str());
        WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
        WiFi.softAP((char *)ap_ssid, ap_password,1,0); //for hidding the AP mode ssid
        delay(1000);
        WiFi.setAutoConnect(true);

        //Starting Websocket
        String temp = "OK";
        webSocket.begin();
        webSocket.broadcastPing(temp);
        webSocket.onEvent(webSocketEvent);

        //Starting UDP Server
        udp.beginMulticast(WiFi.localIP(), ipMulti, 6777);
        udp.beginPacketMulticast(ipMulti, 6777, WiFi.localIP());
        udp.write(WiFi.softAPmacAddress().c_str());
        udp.endPacket();


        //OTA upgrade
        String hostname("Connected-OTA-");
        hostname += String(ESP.getChipId(), HEX);
        ArduinoOTA.setHostname((const char *)hostname.c_str());
        ArduinoOTA.begin();
        irsend.begin();

        //Connecting to RabbitMQ server for MQTT Communications
#if DEBUG
        Serial.println();
        Serial.print("Reading MQTT configuration from EEPROM at the start:");
        Serial.println(read_String(MQTTCONFIG));
#endif
        String e_mqttConfig = read_String(MQTTCONFIG);
        if (e_mqttConfig != NULL && strlen( e_mqttConfig.c_str() ) >0 ) {
                Command_MQTTConfig(e_mqttConfig, 0);
        }
        else{
#if DEBUG
                Serial.println("MQTT Configuration is empty, setup the configuration");
#endif
        }

}

void loop()
{
        int packetSize = udp.parsePacket();
        if (packetSize)
        {
                // read the packet into packetBufffer
                udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
                // send a reply, to the IP address and port that sent us the packet we received
                udp.beginPacket(udp.remoteIP(), udp.remotePort());
                temp = "MAC=" + WiFi.macAddress() + "=" + WiFi.softAPmacAddress() + "=";
                udp.write(temp.c_str());
                udp.endPacket();
                temp = "";
        }
        webSocket.loop();

        if (!mqttClient.connected()) {
                long now = millis();
                if (now - lastReconnectAttempt > 10000) {
                        lastReconnectAttempt = now;
                        // Attempt to reconnect
                        if (mqttClientConnect()) {
                                lastReconnectAttempt = 0;
                        }
                }
        } else {
                // Client connected
                mqttClient.loop();
        }

        //
        while (Serial.available()) {
                TxRx_string = Serial.readString();
                String responseMessage = "CONNECTEDLITE_NODE=" +WiFi.macAddress()+ "=" + WiFi.softAPmacAddress()+ "=" + TxRx_string;
                mqttClient.publish(responseRoutingKey.c_str(), responseMessage.c_str() );
                webSocket.broadcastTXT(responseMessage);
                temp = "";
                TxRx_string = "";
        }
}
