
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

/* Put your SSID & Password */
const char* ssid = "NodeMCU";  // Enter SSID here
const char* password = "12345678";  //Enter Password here

String wifi_ssid;
String wifi_pwd;

/* Put IP Address details */
IPAddress local_ip(192,168,2,1);
IPAddress gateway(192,168,2,1);
IPAddress subnet(255,255,255,0);

ESP8266WebServer server(80);

uint8_t LEDpin = D2;
uint8_t RESETpin = D5;

bool LEDstatus;
int addr_status = 0;

int addr_SSID = 10;
int addr_PWD = 40;

void setup() {  
  Serial.begin(9600);
  delay(100);
  pinMode(LEDpin, OUTPUT);
  pinMode(RESETpin, INPUT);
  attachInterrupt(RESETpin, resetMCU, RISING);
  EEPROM.begin(512);

  String ts = read_String(addr_SSID);
  String tp = read_String(addr_PWD);
  Serial.println("Read data from EEPROM");
  Serial.println(wifi_ssid);  
  Serial.println(wifi_pwd);
  wifi_ssid = ts;
  wifi_pwd = tp;
  
  if ( wifi_ssid.length() > 0 ) {
    setUpWiFiConnection(ts,tp);
  }
  else{
    Serial.println("Error reading SSID/PWD from EEPROM");    
    WiFi.disconnect();  
    WiFi.softAP(ssid, password);
    WiFi.softAPConfig(local_ip, gateway, subnet);
    delay(100);
    Serial.println("Started in AP mode");
  }
  
  server.on("/", handle_OnConnect);
  server.on("/ledon", handle_ledon);
  server.on("/ledoff", handle_ledoff); 
  server.on("/connectToSSID", connectToSSID);
  server.on("/reset", resetMCU);
  server.onNotFound(handle_NotFound);

  EEPROM.get(addr_status,LEDstatus);
  Serial.print("Read status from EEPROM :");
  Serial.println(LEDstatus,DEC);
  if ( LEDstatus ) {   
    Serial.println("Status is HIGH");
  }    
  else {    
    Serial.println("Status is LOW");
  }  
  
  server.begin();
  Serial.println("HTTP server started");
}


void loop() {
  server.handleClient();
  if(LEDstatus)  
    digitalWrite(LEDpin, HIGH);
  else
    digitalWrite(LEDpin, LOW);
}

void connectToSSID() {
  
  wifi_ssid = server.arg("ssid").c_str();
  wifi_pwd = server.arg("password").c_str();
  
  write_String(addr_SSID,wifi_ssid);
  write_String(addr_PWD,wifi_pwd);
  EEPROM.commit();
  
  String ts = read_String(addr_SSID);
  String tp = read_String(addr_PWD);

  Serial.println("Checking the written data");
  Serial.println(ts);  
  Serial.println(tp);
  
  setUpWiFiConnection(wifi_ssid, wifi_pwd);
}

void setUpWiFiConnection(String ssid, String password) {
  Serial.println("");
  Serial.println("---------------------------------------------------------------");  
  Serial.println("Connecting to ");
  Serial.println(ssid);
  Serial.println("Using pasword ");
  Serial.println(password);
  
  //connect to your providde wi-fi network  
  WiFi.begin(ssid.c_str(), password.c_str());
  

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
  delay(1000);
  Serial.print(".");
  }
  
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  Serial.println(WiFi.localIP());
  Serial.println("Turning off AP mode!");
  WiFi.softAPdisconnect (true);
  Serial.println("---------------------------------------------------------------");

  if (LEDstatus)
    server.send(200, "text/html", SendHTML(true));
  else 
    server.send(200, "text/html", SendHTML(false));
}

void handle_OnConnect() {
  EEPROM.get(addr_status,LEDstatus);  
  if (LEDstatus)
    server.send(200, "text/html", SendHTML(true));
  else 
    server.send(200, "text/html", SendHTML(false));
}

void handle_ledon() {
  LEDstatus = HIGH;
  EEPROM.put(addr_status,LEDstatus);
  EEPROM.commit();
  server.send(200, "text/html", SendHTML(true)); 
}

void handle_ledoff() {
  LEDstatus = LOW;
  EEPROM.put(addr_status,LEDstatus);
  EEPROM.commit();
  server.send(200, "text/html", SendHTML(false)); 
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

void resetMCU() {
  server.send(200, "text/html", ResetHTML());   
  Serial.println("D5 is HIGH or Software Reset hence resetting - Clearing WIFI and SSID on EEPROM");  
  
  WiFi.disconnect();  
  
  clear_String(addr_SSID,wifi_ssid);
  clear_String(addr_PWD,wifi_pwd);    
  EEPROM.commit();  
  
  delay(3000);  
  Serial.println("");  
  Serial.println("Restarting MCU.............");  
  Serial.println("");  
  ESP.restart();
  
}

String SendHTML(uint8_t led){
  String ptr = "<!DOCTYPE html>\n";
  ptr +="<html>\n";
  ptr +="<head>\n";
  ptr +="<title>LED Control</title>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>LED</h1>\n";
  ptr +="<p>Click to switch LED on and off.</p>\n";
  ptr +="<form method=\"get\">\n";
  if(led)
    ptr +="<input type=\"button\" value=\"LED OFF\" onclick=\"window.location.href='/ledoff'\">\n";
  else
  ptr +="<input type=\"button\" value=\"LED ON\" onclick=\"window.location.href='/ledon'\">\n";
  ptr +="</form><br><br>";
  ptr +="<form method=\"get\">\n";  
  ptr +="<input type=\"button\" value=\"RESET\" onclick=\"window.location.href='/reset'\">\n";
  ptr +="</form><br><br><br><br><br><br>";
  
  ptr +="<form action=\"connectToSSID\">\n";  
  ptr +="SSID:<br>";
  ptr +="<input type=\"text\" name=\"ssid\"><br>";
  ptr +="Password:<br>";
  ptr +="<input type=\"text\" name=\"password\"><br>";
  ptr +="<input type=\"submit\" value=\"CONNECT\">\n";
  ptr +="</form>";  
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}

String ResetHTML(){
  String ptr = "<!DOCTYPE html>\n";
  ptr +="<html>\n";
  ptr +="<head>\n";
  ptr +="<title>LED Control</title>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>NodeMCU Restting....</h1>\n";  
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}


void write_String(char add,String data)
{
  Serial.println("Writing string");
  int _size = data.length();
  int i;
  for(i=0;i<_size;i++)
  {    
    EEPROM.write(add+i,data[i]);
    EEPROM.commit();
    //Serial.println(add);
  }
  EEPROM.write(add+_size,'\0');   //Add termination null character for String Data  
}
 
 
String read_String(char add)
{  
  int i;
  char data[100]; //Max 100 Bytes
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  //Serial.println("Character : ");  
  //Serial.println(add);
  while(k != '\0' && len<500)   //Read until null character
  {    
    k=EEPROM.read(add+len);
    //Serial.println("Character : ");
    //Serial.println(k);
    data[len]=k;
    len++;
  }
  data[len]='\0';
  return String(data);
}


void clear_String(char add,String data)
{
  int _size = data.length();
  int i;
  for(i=0;i<=_size;i++)
  {
    EEPROM.write(add+i,0);
  }  
}

